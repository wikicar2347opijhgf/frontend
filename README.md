This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

npm run dev

El proyecto funciona en el puerto 4000 (http://localhost:4000/)

- Una vez dentro, se encuentra con el home, click en "Crea el primero!"
- En el formulario llenar: nombre del producto, cantidad y precio del mismo, luego click en guardar
- Se redirecciona al listado de productos el cual se actualiza solo
- Click en algún producto para mostrar mas opciones
- Para actualizar, solo cambie los valores en el formulario y click en "actualizar"
- Para eliminar, dar click en "eliminar" 

Existe un error el cual indica: "Error: Text content does not match server-rendered HTML.
Warning: Text content did not match. Server: "4/22/2023" Client: "22/4/2023""

Solo cierre el modal de error si llega a aparecer, esto es porque uso el dato de la fecha sin un setState ya que ocupo un map para crear
todas las tarjetas y no me funcionaba bien el hook que intenté usar.
