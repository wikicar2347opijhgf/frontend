import { Card, Form, Grid, Button, Icon, Confirm } from "semantic-ui-react";
import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Smartphone } from "../interfaces/smartphone";
import { Layout } from "../components/layout";

const inititalState = {
  NombreSmart: "",
  Modelo: "",
  PrecioRef: 0,
  PrecioV: 0,
  AnMod: 0,
};

const NewPage = (): JSX.Element => {
  const [smart, setSmart] = useState<Smartphone>(inititalState);
  const [loading, setLoading] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const router = useRouter();

  const createSmart = async (smart: Smartphone) =>
    await fetch("http://localhost:3000/api/smartphone", {
      method: "POST",
      body: JSON.stringify(smart),
      headers: {
        "Content-Type": "application/json",
      },
    });

  const updateSmart = async (id: string, smart: Smartphone) =>
    await fetch("http://localhost:3000/api/smartphone/" + id, {
      method: "PUT",
      body: JSON.stringify(smart),
      headers: {
        "Content-Type": "application/json",
      },
    });

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    try {
      let response;
      if (typeof router.query.id === "string") {
        response = await updateSmart(router.query.id, smart);
      } else {
        response = await createSmart(smart);
      }
      const updatedSmart = await response.json();
      setSmart(updatedSmart);
      router.push("/");
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  const handleChange = ({ target: { name, value } }: ChangeInputHandler) =>
    setSmart({ ...smart, [name]: value });

  const loadSmart = async (id: string) => {
    const res = await fetch("http://localhost:3000/api/smartphone/" + id);
    const smart = await res.json();
    setSmart({
      NombreSmart: smart.NombreSmart,
      Modelo: smart.Modelo,
      PrecioRef: smart.PrecioRef,
      PrecioV: smart.PrecioV,
      AnMod: smart.AnMod,
    });
  };

  const handleDelete = async (id: string) => {
    try {
      const res = await fetch("http://localhost:3000/api/smartphone/" + id, {
        method: "DELETE",
      });
      router.push("/");
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (typeof router.query.id === "string") loadSmart(router.query.id);
  }, [router.query]);

  return (
    <Layout>
      <Grid
        textAlign="center"
        style={{ height: "70vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Card fluid color="teal" className="card">
            <Card.Content>
              <Form onSubmit={handleSubmit} size="large">
                <Form.Field>
                  <Form.Input
                    fluid
                    label="Nombre"
                    name="NombreSmart"
                    value={smart.NombreSmart}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    fluid
                    label="Modelo"
                    name="Modelo"
                    value={smart.Modelo}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    fluid
                    label="Precio de Referencia"
                    name="PrecioRef"
                    type="number"
                    value={smart.PrecioRef}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    fluid
                    label="Precio de Venta"
                    name="PrecioV"
                    type="number"
                    value={smart.PrecioV}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    fluid
                    label="Año de Modelo"
                    name="AnMod"
                    type="number"
                    value={smart.AnMod}
                    onChange={handleChange}
                  />
                </Form.Field>
                {router.query.id ? (
                  <Button color="teal" loading={loading} fluid size="large">
                    <Icon name="save" />
                    Actualizar
                  </Button>
                ) : (
                  <Button color="blue" fluid size="large" loading={loading}>
                    <Icon name="save" />
                    Guardar
                  </Button>
                )}
                <Button fluid size="large" onClick={() => router.push("/")}>
                  Cancelar
                </Button>
                {router.query.id && (
                  <Button
                    color="red"
                    onClick={() =>
                      typeof router.query.id === "string" &&
                      handleDelete(router.query.id)
                    }
                    fluid
                    size="large"
                  >
                    <Icon name="trash" />
                    Eliminar
                  </Button>
                )}
              </Form>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    </Layout>
  );
};

export default NewPage;
