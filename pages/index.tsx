import { GetServerSideProps } from "next";
import { Button, Grid } from "semantic-ui-react";
import { useRouter } from "next/router";
import { Layout } from "./components/layout";
import { Smartphone } from "./interfaces/smartphone";
import ProductsList from "./components/smartphonelist";

interface Props {
  smart: Smartphone[];
}

const Home = ({ smart }: Props) => {
  const { push } = useRouter();

  return (
    <Layout>
      <Button onClick={() => push("/smartphone/new")}>
        Agregar SmartPhone
      </Button>
      {smart.length === 0 ? (
        <Grid
          columns={3}
          centered
          verticalAlign="middle"
          style={{ height: "70%" }}
        >
          <Grid.Row>
            <Grid.Column>
              <div style={{ color: "#eee", textAlign: "center" }}>
                <h1>No hay Smartphones</h1>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      ) : (
        <ProductsList smart={smart} />
      )}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const res = await fetch("http://localhost:3000/api/smartphone");
  const smart = await res.json();
  return {
    props: { smart },
  };
};

export default Home;
