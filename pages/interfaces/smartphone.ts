export interface Smartphone {
  id?: number;
  NombreSmart: string;
  Modelo: string;
  PrecioRef: number;
  PrecioV: number;
  AnMod: number;
  AudiData?: Date;
}
