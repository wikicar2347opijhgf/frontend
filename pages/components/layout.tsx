import { Container } from "semantic-ui-react";

const styles = {
  main: {
    backgroundImage: "linear-gradient(135deg, #282c34 0%, #3c3e44 100%)",
    boxShadow: "0px 10px 15px rgba(0, 0, 0, 0.3)",
    borderRadius: "20px",
    padding: "20px",
    overflow: "hidden",
    color: "#f5f5f5",
  },
  container: {
    paddingTop: "2rem",
    height: "90vh",
    overflowY: "auto",
    backgroundColor: "rgba(255, 255, 255, 0.1)",
    borderRadius: "15px",
    padding: "20px",
  },
};

export const Layout = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  return (
    <div>
      <main style={styles.main}>
        <Container style={styles.container}>{children}</Container>
      </main>
    </div>
  );
};
