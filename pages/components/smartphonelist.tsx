import { useRouter } from "next/router";
import { Card, CardMeta } from "semantic-ui-react";
import { Smartphone } from "../interfaces/smartphone";

interface Props {
  smart: Smartphone[];
}

function ProductsList({ smart }: Props) {
  const router = useRouter();
  return (
    <>
      <Card.Group>
        {smart.map((smart) => (
          <Card
            key={smart.id}
            onClick={() => router.push(`/smartphone/edit/${smart.id}`)}
          >
            <Card.Content>
              <Card.Header>{smart.NombreSmart}</Card.Header>
              <Card.Description>
                Precio de Referencia: {smart.PrecioRef} Bs. <br />
                Precio de Venta: {smart.PrecioV} Bs. <br />
              </Card.Description>
              {smart.AudiData && (
                <Card.Description>
                  Año del Modelo: {smart.AnMod} <br /> Datos Auditoria:{" "}
                  {new Date(smart.AudiData).toLocaleDateString()}
                </Card.Description>
              )}
            </Card.Content>
          </Card>
        ))}
      </Card.Group>
      {}
    </>
  );
}

export default ProductsList;
